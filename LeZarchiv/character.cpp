#include "stdafx.h"
#include "character.h"


Character::Character(std::string imgPath, std::string deadimgpath,  SpriteManager* spman, int HP, int damage, bool isBerserk)
	: Physical(imgPath, deadimgpath, spman)
	, hitPoints(HP)
	, dmg(damage)
	, isBerserk(isBerserk)
{
}

Character::Character(pugi::xml_node const tetedenoeud, SpriteManager* spman)
	: Physical(tetedenoeud.attribute("imgpath").as_string(), tetedenoeud.attribute("deadimgpath").as_string(), spman)
	, hitPoints(tetedenoeud.attribute("hp").as_int())
	, dmg(tetedenoeud.attribute("dmg").as_int())
	, isBerserk(tetedenoeud.attribute("isBerserk").as_bool())
{
	std::cout << " " << tetedenoeud.attribute("imgpath").as_string() << " ";
}