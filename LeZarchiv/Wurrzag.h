#pragma once
#include "character.h"

class Wurrzag : public Character {
public :
	sf::FloatRect wurrBounds;
	Wurrzag(SpriteManager* spman);
	int maxHitPoints;
};