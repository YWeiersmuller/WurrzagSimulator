#include "stdafx.h"
#include "wurrzag.h"

Wurrzag::Wurrzag(SpriteManager* spman)
	: Character("../data/img/wurrzag.png", "../data/img/wurrzag_mort.png", spman, 50, 10, true)
	, wurrBounds(sprite->getGlobalBounds())
	, maxHitPoints(50)
{
}