#pragma once
#include "Wurrzag.h" //also includes both physical and character
#include <vector>
#include <SFML/System/Time.hpp>

class WorldDirector {
public:
	SpriteManager spman;

	//properties of the window
	float height;
	float width;
	float groundHeight = 50.f; //"ground" stands at 50px above window bottom
	float absoluteGroundHeight = height - groundHeight;

	//spawn properties
	int maxSpawnTime=6;
	float testInterval; //used by the spawn manager. 
	float lastSpawnTime;

	std::unique_ptr<Wurrzag> wurrzag;
	std::vector<std::unique_ptr<Character>> npcs;
	//std::vector<std::unique_ptr<Physical>> environment;
	
	//moves wurrzag
	//void move();
	//makes wurrzag jump
	float movablejumpHeight = 400.f;
	bool jumpstart; //is TRUE when a jump is initialized. FALSE otherwise
	//float jumpHeight(float time1, float time2);
	void processJump(float time2);

	
	//collisions & hits
	bool onGround();
	bool collisionManager(sf::FloatRect npcBounds, float x, float y);
	float hitFrequency = 0.5f;
	float lastHitTime;

	//creates an new NPC
	void NPCspawner();
	//Manages spawn time
	void spawnManager(float timestep);

	/*//score and HP counters
	float lastUpdateTime;
	float updateTime = 0.2;
	sf::Font font;
	sf::Text score;
	sf::Text hitPoints;*/

	//advances time and manages what goes along
	void update(float timestep, float timestamp);

	

	WorldDirector();
	WorldDirector(int height, int width);
};