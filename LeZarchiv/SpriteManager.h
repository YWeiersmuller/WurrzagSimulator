#pragma once
#include <map>
#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>

class SpriteManager
{
public:
	SpriteManager();

	std::map<std::string, sf::Texture> textures;
	bool isloaded(std::string path);
	void loadtexture(std::string path);
	std::shared_ptr<sf::Sprite> gettexture(std::string path);
};

