#include "stdafx.h"
#include "SpriteManager.h"


SpriteManager::SpriteManager()
{
}
bool SpriteManager::isloaded(std::string path)
{
	if (textures.find(path) != textures.end())
	{
		return true;
	}
	return false;
}
void SpriteManager::loadtexture(std::string path)
{
	auto texture = sf::Texture();
	if (!texture.loadFromFile(path))
	{
		std::cout << "Error texture from file path : " << path << std::endl;
	}
	textures[path] = texture;
}
std::shared_ptr<sf::Sprite> SpriteManager::gettexture(std::string path)
{ 
	if (!isloaded(path)) {
		loadtexture(path);
	}
	auto sprite = std::make_shared<sf::Sprite>();
	sprite->setTexture(textures[path], true); //"true" gets the sprite to be resized to the dimension of the texture used
	return std::move(sprite);
}

