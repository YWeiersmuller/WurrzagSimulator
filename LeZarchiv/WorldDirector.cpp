#include "stdafx.h"
#include "WorldDirector.h"
#include <random>


// Fonction simulant un de a nbSides faces.
int d(int const nbSides)
{
	static std::random_device rd;
	static std::default_random_engine engine(rd());
	std::uniform_int_distribution<> distribution(1, nbSides);
	return distribution(engine);
}
//the function of the parabole made during the jump
float f(float x)
{
	return -4.f * x * (x - 1);
}
//returns true if derivative of f is positive, false if not
bool signFDerivative(float x) {
	if (x < 0.5) {
		return true;
	}
	else {
		return false;
	}
}

WorldDirector::WorldDirector()
	: height(0)
	, width(0)
	, lastSpawnTime(0)
	, wurrzag(std::make_unique<Wurrzag>(&spman))
{
	
}

WorldDirector::WorldDirector(int height, int width)
	: height(height)
	, width(width)
	, lastSpawnTime(0.f)
	, wurrzag(std::make_unique<Wurrzag>(&spman))
{
	this->wurrzag->setHeight(height, groundHeight);
}

/*float WorldDirector::jumpHeight(float time1, float time2)
{
	if (time2 - time1 > 1.f) {
		std::cout << " [jump not resolved in a sec] ";
		return 0; //a jump is resolved in 1 sec. (if this case happens, sucks to be me)
	}
	if (time2 - time1 < 0.f) {
		std::cout << " [jump not resolved in a sec] ";
		return 0; //if this case happens, error somewhere
	}
	return (f(time2) - f(time1))*movablejumpHeight;
}*/

bool WorldDirector::onGround()
{
	//if wurrzag is on the ground
	float difference = wurrzag->sprite->getPosition().y - (height - groundHeight);
	if (difference < 1.f && difference > -1.f)
	{
		//std::cout << "Wurrzag is on the ground; ";
		return true;
	}
	return false;
}

void WorldDirector::processJump(float time2)
{
	//std::cout << "processing jump begins; ";
	int distance = f(time2)*movablejumpHeight;
	//std::cout << "jump height calculated for " << time2 << " : " << distance << ", moving sprite...; ";
	//we consider hitting the ground when the next jump step brings wurrzag close enough or below the groundline
	//and we check if he is indeed going down with the derivative of f
	//so wurrzag does not get stuck at the begninning of his jump
	if (distance < 1.f && !signFDerivative(time2)) {
		//std::cout << "stepping on ground again; ";
		wurrzag->sprite->setPosition(wurrzag->sprite->getPosition().x, absoluteGroundHeight);
	}
	else {
		wurrzag->sprite->setPosition(wurrzag->sprite->getPosition().x, absoluteGroundHeight-distance);
		//std::cout << "sprite moved of : " << distance << " pixels ; ";
	}
	//std::cout << "sprite moved, moving b2body...; ";
	//wurrzag->body->SetTransform(b2Vec2(wurrzag->body->GetPosition().x, distance + wurrzag->body->GetPosition().y), 0.f);
}

void WorldDirector::NPCspawner()
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file("../data/characters.xml");
	if (result) {
		std::cout << " | successfully loaded xml file; ";
	}
	pugi::xml_node npc;
	if (result)
	{
		int dice = d(7);
		if (dice < 5) {
			std::cout << "creating dwarf ";
			npc = doc.child("character").child("dwarf");
		}
		if (dice == 5 || dice == 6)
		{
			std::cout << "creating slayer ";
			npc = doc.child("character").child("slayer");
		}
		if (dice == 7)
		{
			std::cout << "creating ork ";
			npc = doc.child("character").child("ork");
		}
		auto npc_ptr = std::make_unique<Character>(npc, &spman);
		npc_ptr->setHeight(height, groundHeight);
		npc_ptr->sprite->setPosition(width, npc_ptr->sprite->getPosition().y);
		npcs.push_back(std::move(npc_ptr));
	}
}

void WorldDirector::spawnManager(float timestep) {
	lastSpawnTime = lastSpawnTime + timestep;
	//std::cout << "time since last test : " << testInterval << " ; ";
	if (testInterval >= 0.5) {
		testInterval = 0.f;
		int spawn = d(maxSpawnTime);
		//std::cout << " lastspawntime " << lastSpawnTime << " VS random number : " << (float)spawn << " ; ";
		if (lastSpawnTime >= (float)spawn)
		{
			//std::cout << " [NEW NPC] ; ";
			lastSpawnTime = 0.f;
			NPCspawner();
		}
	}
	else {
		//std::cout << " adding " << timestep << " seconds ; ";
		testInterval = testInterval + timestep;
	}
}

/*bool WorldDirector::collisionManager(sf::FloatRect npcBounds) {
	if (npcBounds.intersects(wurrzag->wurrBounds)) {
		std::cout << "collision ; ";
		return true;
	}
	else {
		return false;
	}
}*/

bool WorldDirector::collisionManager(sf::FloatRect npcBounds, float x, float y) {
	//manual, raw test :
	//npc_x - wur_width < wur_posX < npc_x  + npc_width
	//npc_y - wur_height < wur_posY < npc_y + npc_height
	if ((x - (wurrzag->wurrBounds.width) < wurrzag->sprite->getPosition().x)
		&&(wurrzag->sprite->getPosition().x < x + npcBounds.width)
		&&(y - (wurrzag->wurrBounds.height) < wurrzag->sprite->getPosition().y)
		&& (wurrzag->sprite->getPosition().y < y + npcBounds.height)){

		std::cout << "collision ; ";
		return true;
	}
	else {
		return false;
	}
}

void WorldDirector::update(float timestep, float time)
{
	lastHitTime = lastHitTime + timestep;
	for (auto& npc : npcs) {

		if (npc->hitPoints > 0 && collisionManager(npc->sprite->getGlobalBounds(), npc->sprite->getPosition().x, npc->sprite->getPosition().y)) {
			if (!signFDerivative(time) && (!onGround())) {//if we are falling on a target, cause dmg
				npc->hitPoints = npc->hitPoints - wurrzag->dmg;
				jumpstart = true; //jumping again
				if (npc->isBerserk && lastHitTime >= hitFrequency) { //
					lastHitTime = 0.f;
					wurrzag->hitPoints = wurrzag->hitPoints - npc->dmg;
					if (wurrzag->hitPoints > 50) {
						//lifecap, literaly
						wurrzag->hitPoints = 50;
					}
				}
			}
			else {
				if (lastHitTime >= hitFrequency) { //we are not attacking, we hence "take damage"
					lastHitTime = 0.f;
					wurrzag->hitPoints = wurrzag->hitPoints - npc->dmg;
					if (wurrzag->hitPoints > 50) {
						//lifecap, literaly
						wurrzag->hitPoints = 50;
					}
				}
			}
		}
		/*if (npc->sprite->getLocalBounds().width >= -npc->sprite->getPosition().x)
		{
			//std::cout << "removing a npc ; ";
			npcs.erase(std::find(npcs.begin(), npcs.end(), npc));
			//std::cout << "npc removed ; ";
		}*/
		npc->sprite->move(-640.f*timestep, 0.f);
		
	}
	spawnManager(timestep);
}