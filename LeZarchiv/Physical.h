#pragma once
#include "SpriteManager.h"

class Physical {
public:
	std::shared_ptr<sf::Sprite> sprite;
	std::shared_ptr<sf::Sprite> deadsprite;
	Physical(std::string imgPath, std::string deadimgpath, SpriteManager* spman);
	void setHeight(float windowHeight, float groundHeight);
};