#pragma once
#include "Physical.h"
#include "pugixml.hpp"

class Character : public Physical {
public:
	int hitPoints;
	int dmg;
	bool isBerserk;
	Character(std::string imgPath, std::string deadimgpath, SpriteManager* spman, int HP, int dmg, bool isBerserk);
	Character(pugi::xml_node const tetedenoeud, SpriteManager* spman);
};