#include "stdafx.h"
#include "Physical.h"

Physical::Physical(std::string imgpath, std::string deadimgpath, SpriteManager* spman)
	: sprite(spman->gettexture(imgpath))
	, deadsprite(spman->gettexture(deadimgpath))
{
}

void Physical::setHeight(float windowHeight, float groundHeight)
{
	sf::FloatRect spriteRect = sprite->getLocalBounds();
	float spriteHeight = spriteRect.height;
	sprite->setOrigin(0.f, spriteHeight);
	deadsprite->setOrigin(0.f, deadsprite->getLocalBounds().height);
	sprite->setPosition(50.f, windowHeight-groundHeight );
}