#include "stdafx.h"
#include "eulMain.h"

int eulMain()
{
	//initializing clock
	sf::Clock clock;
	float timestep1=-1.f;
	float timestep2=-1.f;

	bool gameOver = true;
	sf::RenderWindow window(sf::VideoMode(1280, 720), "F'netr' Eud'Jeu");
	window.setFramerateLimit(100);
	
	//creating world and bckg images
	WorldDirector eulMonde = WorldDirector(window.getSize().y, window.getSize().x);

	sf::Texture startText;
	if (!startText.loadFromFile("../data/img/gamestart.png"))
	{
		std::cout << "Failed loading start image" << std::endl;
	}
	sf::Sprite startSprite;
	startSprite.setTexture(startText);

	sf::Texture bgtexture;
	if (!bgtexture.loadFromFile("../data/img/bckg.jpg"))
	{
		std::cout << "Failed loading background image" << std::endl;
	}
	sf::Sprite Bgsprite;
	Bgsprite.setTexture(bgtexture);
	
	sf::Texture gameOverText;
	if (!gameOverText.loadFromFile("../data/img/gameover.png"))
	{
		std::cout << "Failed loading gameover image" << std::endl;
	}
	sf::Sprite GOsprite;
	GOsprite.setTexture(gameOverText);

	std::cout << "Intializing done" << std::endl;
	
	while (window.isOpen())
	{
		std::cout << "FRAME treatment begins : ";
		window.clear();
		
		//EVENT TREATMENT BEGINS HERE
		//std::cout << "treating event; ";
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				std::cout << "closing window" << std::endl;
				window.close();
			}
			if (event.type == sf::Event::KeyPressed) 
			{
				//jumps if space key pressed AND if in collision (with ground or other similar elements)
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) 
				{
					std::cout << "closing window with escape command" << std::endl;
					window.close();
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) 
				{
					std::cout << "[ jump key pressed ] ";
					if (eulMonde.wurrzag->hitPoints > 0 && eulMonde.onGround() && !gameOver) 
					{
						eulMonde.jumpstart = true;
					}
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter)) 
				{
					if (gameOver) 
					{
						gameOver = false;
						eulMonde.jumpstart = false;
						eulMonde.npcs.clear();
						eulMonde.wurrzag->hitPoints = 50;
						eulMonde.wurrzag->setHeight(eulMonde.height, eulMonde.groundHeight);
					}
				}
				/*if (sf::Keyboard::isKeyPressed(sf::Keyboard::C)) {
					eulMonde.npcs.clear();
				}*/
			}
		}

		if (gameOver && timestep1 == -1.f) //only happens at the start
		{
			window.draw(startSprite);
		}

		else {
			if (!gameOver)
			{
				//MISC OPERATIONS ON WOLRD ELTS
				//std::cout << "beginning world treatment; ";
				//if in mid-jump / beginning jump
				if (eulMonde.jumpstart)
				{
					//std::cout << "resetting jump clock ; ";
					clock.restart();
					timestep1 = 0.f;
					eulMonde.jumpstart = false;
				}
				else
				{
					if (!eulMonde.onGround() || timestep1 == 0.f)
					{
						//std::cout << "treating a jump step; ";
						timestep2 = clock.getElapsedTime().asSeconds();
						eulMonde.processJump(timestep2);
						//std::cout << "updating world; ";
						eulMonde.update(timestep2 - timestep1, timestep2);
						timestep1 = timestep2;
						//std::cout << "updated; ";
					}
					else
					{
						//std::cout << "no jumps; ";
						timestep2 = clock.getElapsedTime().asSeconds();
						//std::cout << "updating world; ";
						eulMonde.update(timestep2 - timestep1, timestep2);
						timestep1 = timestep2;
						//std::cout << "updated; ";
					}
				}


				//WINDOW & SPRITE TREATMENT BEGINS HERE
				//std::cout << "beginning window updating; ";
				// c'est ici qu'on dessine tout
				window.draw(Bgsprite);
				std::cout << "currently managing " << eulMonde.npcs.size() << " npcs; ";
				for (auto& npc : eulMonde.npcs)
				{
					//std::cout << "drawing a npc; ";

					if (npc->hitPoints <= 0) {
						//special treatment !
						//changing deadsprite origin so thats it appears with its bottom left corner in the same place
						//as the livingsprite's bottom left corner (which is the sprites' origins)
						npc->deadsprite->setPosition(npc->sprite->getPosition());
						window.draw(*(npc->deadsprite));
					}
					else {
						window.draw(*(npc->sprite));
					}
				}
				if (eulMonde.wurrzag->hitPoints <= 0) {
					gameOver = true;
					clock.restart();
					eulMonde.wurrzag->deadsprite->setPosition(eulMonde.wurrzag->sprite->getPosition());
					window.draw(*(eulMonde.wurrzag->deadsprite));
				}
				else {
					window.draw(*(eulMonde.wurrzag->sprite));
				}
			}
			if (gameOver && clock.getElapsedTime().asSeconds() < 1.f)
			{
				window.draw(Bgsprite);
				std::cout << "drawing GAME OVER sprite ; ";
				for (auto& npc : eulMonde.npcs)
				{
					if (npc->hitPoints <= 0)
					{
						//special treatment !
						//changing deadsprite origin so thats it appears with its bottom left corner in the same place
						//as the livingsprite's bottom left corner (which is the sprites' origins)
						npc->deadsprite->setPosition(npc->sprite->getPosition());
						window.draw(*(npc->deadsprite));
					}
					else
					{
						window.draw(*(npc->sprite));
					}
					window.draw(*(eulMonde.wurrzag->deadsprite));
				}
			}
			if (gameOver && clock.getElapsedTime().asSeconds() > 1.f) {
				std::cout << "drawing GAME OVER sprite ; ";
				window.draw(GOsprite);
			}

		}
		
		window.display();
		std::cout << "FRAME DONE." << std::endl;

	}

	return 0;
}