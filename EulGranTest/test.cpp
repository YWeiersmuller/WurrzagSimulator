#include "pch.h"
#include "../LeZarchiv/WorldDirector.h"

TEST(TestSpriteManager, TestSpriteManager) {
	SpriteManager spman = SpriteManager();
	std::string path = "../data/img/bckg.jpg";
	spman.loadtexture(path);
	EXPECT_EQ(spman.textures.size(), 1);
	EXPECT_TRUE(spman.isloaded(path));
}
/*
TEST(TestReadWML, TestCharacter)
{
	SpriteManager spman = SpriteManager();
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file("../data/characters.xml");
	if (result) {
		pugi::xml_node dwarf;
		b2BodyDef bodyDef;
		bodyDef.type = b2_staticBody;
		dwarf = doc.child("character").child("dwarf");
		Character nain = Character(dwarf, bodyDef, &spman);
		EXPECT_EQ(nain.hitPoints, 10);
		EXPECT_EQ(nain.dmg, 10);
		EXPECT_FALSE(nain.isBerserk);
	}
}*/
TEST(TestWorldConstructor, TestWorld) {
	WorldDirector eulMonde = WorldDirector(720.f, 1280.f);
	EXPECT_EQ(eulMonde.height, 720.f);
	EXPECT_EQ(eulMonde.width, 1280.f);
	//EXPECT_EQ(&(eulMonde.wurrzag)->bodyDef.type, b2_dynamicBody);
	EXPECT_EQ((eulMonde.wurrzag)->hitPoints, 50);
	EXPECT_EQ((eulMonde.wurrzag)->dmg, 10);
	EXPECT_EQ((eulMonde.wurrzag)->isBerserk, true);
	EXPECT_EQ((eulMonde.wurrzag)->sprite->getPosition().y, 720.f - 50.f);
	EXPECT_EQ((eulMonde.wurrzag)->sprite->getPosition().x, 50.f);
}

/*TEST(TestWorldjumpHeight, TestWorld) {
	WorldDirector eulMonde = WorldDirector(720.f, 1280.f);
	EXPECT_EQ(eulMonde.jumpHeight(0.f, 0.f), 0.f);
	EXPECT_EQ(eulMonde.jumpHeight(0.f, 0.5), 400.f); //reach jump summit (400px) in 0.5 seconds
	EXPECT_EQ(eulMonde.jumpHeight(0.f, 1.f), 0.f);

	//testing equality doesn't work : error of circa 1/100 000 (lol)
	EXPECT_LE(eulMonde.jumpHeight(0.2, 0.5)-144.f, 1.f);
	EXPECT_GE(eulMonde.jumpHeight(0.2, 0.5) - 144.f, -1.f);
	EXPECT_LE(eulMonde.jumpHeight(0.5, 0.8)+144.f, 1.f); 
	EXPECT_GE(eulMonde.jumpHeight(0.5, 0.8) + 144.f, -1.f);

	//marginal cases :1
	EXPECT_EQ(eulMonde.jumpHeight(123, 125), 0.f);
	EXPECT_EQ(eulMonde.jumpHeight(125, 123), 0.f);
}*/

TEST(TestOnGround, TestWorld) {
	WorldDirector eulMonde = WorldDirector(720.f, 1280.f);
	EXPECT_TRUE(eulMonde.onGround());
	eulMonde.wurrzag->sprite->move(0.f, 5.f);
	EXPECT_FALSE(eulMonde.onGround());
}
TEST(TestGenerateNPC, TestCreated) {
	WorldDirector eulMonde = WorldDirector(720.f, 1280.f);
	for (int i = 1; i < 15; i++) {
		eulMonde.NPCspawner();
		EXPECT_EQ(eulMonde.npcs.size(), i);
	}
}
TEST(TestGenerateNPC, TestRightStartingPosition) {
	WorldDirector eulMonde = WorldDirector(720.f, 1280.f);
	eulMonde.NPCspawner();
	EXPECT_EQ(eulMonde.npcs[0]->sprite->getPosition().x, 1280.f);
}

TEST(TestRemoveNPC, TestWorld) {
	WorldDirector eulMonde = WorldDirector(720.f, 1280.f);
	eulMonde.NPCspawner();
	EXPECT_EQ(eulMonde.npcs.size(), 1);
	eulMonde.npcs[0]->hitPoints = 0;
	eulMonde.update(0.f, 0.f);
	EXPECT_EQ(eulMonde.npcs.size(), 0);
	for (int i = 1; i < 15; i++) {
		eulMonde.NPCspawner();
	}
	/*EXPECT_EQ(eulMonde.npcs.size(), 14);
	eulMonde.npcs[0]->hitPoints = 0;
	eulMonde.update(0.f, 0.f);
	EXPECT_EQ(eulMonde.npcs.size(), 13);*/
	//eulMonde.update(0.f, 0.f);
	//eulMonde.npcs[0]->hitPoints = 0;
	//eulMonde.npcs[1]->sprite->setPosition(-eulMonde.npcs[1]->sprite->getLocalBounds().width, eulMonde.npcs[1]->sprite->getPosition().y);

}

TEST(TestCollision, TestNoCollision) {
	WorldDirector eulMonde = WorldDirector(720.f, 1280.f);
	eulMonde.NPCspawner();
	//EXPECT_FALSE(eulMonde.npcs[0]->sprite->getLocalBounds().intersects(eulMonde.wurrzag->wurrBounds));
	EXPECT_FALSE(eulMonde.collisionManager(eulMonde.npcs[0]->sprite->getGlobalBounds(), eulMonde.npcs[0]->sprite->getPosition().x, eulMonde.npcs[0]->sprite->getPosition().y));
}
TEST(TestCollision, TestFullCollision) {
	WorldDirector eulMonde = WorldDirector(720.f, 1280.f);
	eulMonde.NPCspawner();
	eulMonde.npcs[0]->sprite->setPosition(eulMonde.wurrzag->sprite->getPosition());
	EXPECT_TRUE(eulMonde.collisionManager(eulMonde.npcs[0]->sprite->getGlobalBounds(), eulMonde.npcs[0]->sprite->getPosition().x, eulMonde.npcs[0]->sprite->getPosition().y));
}
TEST(TestCollision, TestSlightCollision) {
	WorldDirector eulMonde = WorldDirector(720.f, 1280.f);
	eulMonde.NPCspawner();
	eulMonde.npcs[0]->sprite->setPosition(eulMonde.wurrzag->sprite->getPosition().x + (eulMonde.wurrzag->sprite->getPosition().x - 1), eulMonde.wurrzag->sprite->getPosition().y);
	EXPECT_TRUE(eulMonde.collisionManager(eulMonde.npcs[0]->sprite->getGlobalBounds(), eulMonde.npcs[0]->sprite->getPosition().x, eulMonde.npcs[0]->sprite->getPosition().y));
}